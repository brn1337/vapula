# Vapula ![](img/logo-small.png)

> Inspired by [Siofra](https://github.com/cys3c/Siofra)

Vapula scans the system to find exploitable DLL hijacking vulnerabilities. 
Only vulnerabilites exploitable by the current user in the actual system configuration will be shown. 
It can also identify executables runned at startup (persistance) or autoelevated (UAC). 
It is advised to use ProcMon/PoC to confirm vulnerabilites.

Type of vulnerabilities identified:

- DLL replacement
- Ghost/Phantom DLL
- DLL search order

How it works:

- Extract the list of DLLs loaded by an executable using the Import Address Table
- Removes the following: KnownDLLs, WinSxS, manifest explicit DLLs
- Test if any of the directories in DllSearchOrder is writable
- Test if any of the DLL loaded is writable
- Check if the executable has AutoElevate

# Installation

```
git clone https://gitlab.com/brn1337/vapula.git
cd vapula
pip3 install -r requirements.txt
```

# Usage

Scan executables in various locations

```batch
python3 vapula.py -p -d C:\Windows\System32\ -e C:\a.exe C:\b.exe
```

Scan executables runned at startup (stealthy persistance)

```batch
python3 vapula.py -s
```

Scan executables with AutoElevate in C: (UAC bypass)

```batch
python3 vapula.py -d C: -a
```

# References

- https://www.wietzebeukema.nl/blog/hijacking-dlls-in-windows
- https://itm4n.github.io/
- https://lucasg.github.io/2017/06/07/listing-known-dlls/
- https://github.com/cys3c/Siofra
- https://github.com/SeanPesce/DLL_Wrapper_Generator

# License

Vapula is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1)

```
CC-BY-NC-SA This license requires that reusers give credit to the creator.
It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only.
If others modify or adapt the material, they must license the modified material under identical terms.
- Credit must be given to you, the creator.
- Only noncommercial use of your work is permitted.
- Adaptations must be shared under the same terms.
```
